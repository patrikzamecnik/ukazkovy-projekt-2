<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Vrať jim jiskru</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover">
    <meta name="author" content="SIMPLO s.r.o.">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#00a72f">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="msapplication-TileImage" content="/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <script>
      document.documentElement.className =
        document.documentElement.className.replace("no-js", "js");
    </script>
    <link rel="stylesheet" href="/css/main.css">
</head>
<body>
<section class="vj-main-hero">
  <span class="vj-main-hero__spark vj-main-hero__spark--no-anim">
    <img src="/images/jiskry_bottom.jpg" alt="">
  </span>
  <span class="vj-main-hero__spark vj-main-hero__spark--1">
    <img src="/images/jiskry_empty.jpg" alt="">
  </span>
  <span class="vj-main-hero__spark vj-main-hero__spark--2">
    <img src="/images/jiskry_sm.jpg" alt="">
  </span>
  <span class="vj-main-hero__spark vj-main-hero__spark--3">
    <img src="/images/jiskry_empty_big.jpg" alt="">
  </span>
  <span class="vj-main-hero__spark vj-main-hero__spark--main">
    <img src="/images/jiskry_big.jpg" alt="">
  </span>
    <div class="vj-header">
        <div class="vj-header__img">
            <img src="/images/logo1.svg" alt="logo1" class="vj-header__img-el">
        </div>
        <div class="vj-header__img">
            <img src="/images/logo2.svg" alt="logo2" class="vj-header__img-el vj-header__img-el--bf">
        </div>
    </div>
    <div class="vj-main-hero__content text-center">
        <div class="vj-main-hero__inside-box">
            <h1 class="vj-main-hero__title">Zadejte prosím kód,<br>který jsme Vám poslali na email:</h1>
            {!! Form::open( ['route' => ['handleWelcome']] ) !!}
                <input type="text" class="vj-input-line" name="code" required>
                @if ($errors->any())
                  <ul class="vj-error-list">
                      @foreach ($errors->all() as $error)
                          <li class="vj-error-list__item">{{ $error }}</li>
                      @endforeach
                  </ul>
                @endif
                <button class="vj-btn vj-u-mb-hero vj-u-mt-6">Vstoupit</button>
            {!! Form::close() !!}
            <p class="vj-hash-text">#vratjimjiskru</p>
        </div>
    </div>
</section>
<script src="/js/app.js"></script>
</body>
</html>
