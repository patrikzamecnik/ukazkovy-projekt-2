/* global process */

const mix = require('laravel-mix');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const CopyWebpackPlugin = require('copy-webpack-plugin');
const imageminMozjpeg = require('imagemin-mozjpeg');
const path = require('path');


const sourcesPath = 'resources';
const distPath = 'public';

mix.webpackConfig({
    plugins: [
        new CopyWebpackPlugin([{
            from: sourcesPath + '/images',
            to: 'images',
        }]),
        new ImageminPlugin({
            disable: process.env.NODE_ENV !== 'production', // Disable during development
            pngquant: {
                quality: '95-100',
            },
            jpegtran: null,
            svgo: {
              plugins: [{
                cleanupIDs: false
              }]
            },
            plugins: [
                imageminMozjpeg({
                    quality: 80,
                    progressive: true
                })
            ],

            test: /\.(jpe?g|png|gif|svg)$/i
        }),
    ],
});

mix.js(sourcesPath + '/js/app.js', distPath + '/js')
    .sass(sourcesPath + '/scss/main.scss', distPath + '/css')
    .setPublicPath(distPath)
    .copy(sourcesPath + '/fonts', distPath + '/fonts')
    .browserSync({
        proxy: null,
        server: {
            base: './'
        },
        notify: false,
        files: [
            'resources/view/**/*.php',
            'resources/_static/**/*.php',
            distPath + '/js/**/*.js',
            distPath + '/css/**/*.css'
        ],
    });


    mix.options({
        processCssUrls: false
      });

mix.version();
mix.sourceMaps(false, 'source-map');
