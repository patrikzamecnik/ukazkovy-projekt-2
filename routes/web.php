<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SparkController@welcome')->name('welcome');
Route::post('/handleWelcome', 'SparkController@handleWelcome')->name('handleWelcome');

Route::get('form', 'SparkController@form')->name('form');
Route::post('handleForm', 'SparkController@handleForm')->name('handleForm');

Route::get('thanks', 'SparkController@thanks')->name('thanks');
