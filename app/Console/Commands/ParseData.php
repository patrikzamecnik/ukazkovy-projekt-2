<?php

namespace App\Console\Commands;

use App\Ticket;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use League\Csv\Reader;
use League\Csv\Statement;

class ParseData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate data from csv file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \League\Csv\Exception
     */
    public function handle()
    {
        $csv = Reader::createFromPath(public_path("users.csv"), 'r');
        $csv->setHeaderOffset(0); //set the CSV header offset
        $stmt = (new Statement());
        $records = $stmt->process($csv);

        foreach ($records as $record) {
            $user = new User();
            $user->fill([
                'name' => $record['name'],
                'email' => $record['email'],
                'password' => Hash::make(Str::random(8)),
            ]);

            $user->save();

            $ticket = new Ticket();

            $ticket->fill([
                'code' => Str::random(8),
                'is_active' => true,
                'user_id' => $user->id,
            ]);

            $user->tickets()->save($ticket);
        }
    }
}
