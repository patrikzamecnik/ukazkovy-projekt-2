<?php

namespace App\Console\Commands;

use App\Mail\TicketMail;
use App\Ticket;
use Illuminate\Console\Command;
use Illuminate\Contracts\Mail\Mailer;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:sendEmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails with codes';


    /**
     * @var \Illuminate\Contracts\Mail\Mailer
     */
    private $mailer;

    /**
     * Create a new command instance.
     *
     * @param \Illuminate\Contracts\Mail\Mailer $mailer
     */
    public function __construct(Mailer $mailer)
    {
        parent::__construct();

        $this->mailer = $mailer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $tickets = Ticket::with('user')->get();
        foreach ($tickets as $ticket) {
            $this->mailer
                ->to($ticket->user->email)
                ->send(new TicketMail($ticket));
        }
    }
}
