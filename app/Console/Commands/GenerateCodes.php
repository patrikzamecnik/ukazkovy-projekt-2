<?php

namespace App\Console\Commands;

use App\Ticket;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class GenerateCodes extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:codes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        for ($i = 0; $i < 85; $i++) {
            $user = new User();

            $user->fill([
                'name' => Str::random(5),
                'email' => Str::random(5) . '@' . Str::random(5) . '.cz',
                'password' => Hash::make(Str::random(8)),
            ]);

            $user->save();

            $ticket = new Ticket();

            $ticket->fill([
                'code' => Str::random(5),
                'is_active' => true,
                'user_id' => $user->id,
            ]);

            $user->tickets()->save($ticket);
        }
    }
}
