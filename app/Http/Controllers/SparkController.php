<?php

namespace App\Http\Controllers;

use App\Enums\OrganizationsEnum;
use App\History;
use App\Ticket;
use App\User;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

final class SparkController extends Controller
{
    public const SESSION_TICKET_KEY = 'ticket_code';

    /**
     * @var \Illuminate\Contracts\Session\Session
     */
    private $session;


    /**
     * SparkController constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }


    public function welcome()
    {
        return view('welcome');
    }

    public function handleWelcome(Request $request)
    {
        $code = $request->get('code');

        Validator::make($request->all(), [
            'code' => 'required|string|code_exists',
        ])->validated();

        $this->session->put(self::SESSION_TICKET_KEY, $code);

        return redirect()->route('form');
    }

    public function form()
    {
        $code = $this->session->get(self::SESSION_TICKET_KEY);

        /** @var Ticket $ticket */
        $ticket = Ticket::query()->where('code', $code)->first();

        if ($ticket === null || $ticket->isInactive()) {
            return redirect()->route('welcome');
        }

        $user = User::query()->findOrFail($ticket->getAttribute('user_id'));

        return view('form', [
            'fullname' => $user->getAttribute('name'),
        ]);
    }

    public function handleForm(Request $request)
    {
        Validator::make($request->all(), [
            'option' => [
                'required',
                Rule::in(OrganizationsEnum::$organizations)
            ]
        ])->validated();

        $code = $this->session->get(self::SESSION_TICKET_KEY);
        $ticket = Ticket::query()->where('code', $code)->first();

        $history = new History();
        $history->setAttribute('message', $request->get('message'));
        $history->setAttribute('ticket_id', $ticket->getAttribute('id'));
        $history->setAttribute('user_id', $ticket->getAttribute('user_id'));

        if ($request->get('anonym') === 'on') {
            $history->setAttribute('anonym', true);
        } else {
            $history->setAttribute('anonym', false);
        }

        switch ($request->input('option')) {
            case OrganizationsEnum::ALL:
                $history->setAttribute('organization', OrganizationsEnum::ALL);
                break;

            case OrganizationsEnum::ORGANIZATION_1:
                $history->setAttribute('organization', OrganizationsEnum::ORGANIZATION_1);
                break;

            case OrganizationsEnum::ORGANIZATION_2:
                $history->setAttribute('organization', OrganizationsEnum::ORGANIZATION_2);
                break;

            case OrganizationsEnum::ORGANIZATION_3:
                $history->setAttribute('organization', OrganizationsEnum::ORGANIZATION_3);
                break;

            case OrganizationsEnum::ORGANIZATION_4:
                $history->setAttribute('organization', OrganizationsEnum::ORGANIZATION_4);
                break;

            case OrganizationsEnum::ORGANIZATION_5:
                $history->setAttribute('organization', OrganizationsEnum::ORGANIZATION_5);
                break;
        }

        $history->save();
        $ticket->update([
            'is_active'  => 0
        ]);

        $this->session->forget(self::SESSION_TICKET_KEY);

        return redirect()->route('thanks');
    }

    public function thanks()
    {
        return view('thanks');
    }
}
