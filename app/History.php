<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $fillable = [
        'organization', 'anonym', 'message',
        'user_id', 'ticket_id'
    ];

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'history';
}
