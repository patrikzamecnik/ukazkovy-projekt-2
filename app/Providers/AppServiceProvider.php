<?php

namespace App\Providers;

use App\Ticket;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Validator::extend('code_exists', function ($attribute, $value, $parameters, $validator) {
            $ticket = Ticket::query()->where('code', $value)->first();

            if ($ticket === null || $ticket->isInactive()) {
                return false;
            }

            return true;
        });
    }
}
