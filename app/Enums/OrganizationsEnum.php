<?php

namespace App\Enums;

class OrganizationsEnum
{
    const ALL = 'all';
    const ORGANIZATION_1 = 'org1';
    const ORGANIZATION_2 = 'org2';
    const ORGANIZATION_3 = 'org3';
    const ORGANIZATION_4 = 'org4';
    const ORGANIZATION_5 = 'org5';

    public static $organizations = [
        self::ALL,
        self::ORGANIZATION_1,
        self::ORGANIZATION_2,
        self::ORGANIZATION_3,
        self::ORGANIZATION_4,
        self::ORGANIZATION_5
    ];
}
